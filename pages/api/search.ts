// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import {ErrorResponseData, Movie, Paging} from "../../models/common";
import {ConfigService} from "./config";

const configService = ConfigService.getInstance();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Paging<Movie> | ErrorResponseData>
) {
  const searchQuery = req.query.q as string;
  console.log('query, searchQuery');
  const tmdbConfig = await configService.getConfig();

  if (searchQuery?.length) {
      searchMovie(searchQuery).then(data => {
        data.results.map(item => {
          // build image path on clientside
          item.backdropURL = `${tmdbConfig.images.base_url}${tmdbConfig.images.poster_sizes[4]}${item.backdrop_path}`;
          item.posterURL = `${tmdbConfig.images.base_url}${tmdbConfig.images.poster_sizes[4]}${item.poster_path}`;
        })
        res.status(200).json(data);
      });
  } else {
    res.status(400).send({ message: "Query parameter is empty or missing."});
  }
}

function transformQuery(query: string) : string {
  return query
      .split(' ')
      .map((word) => word[0].toUpperCase() + word.substring(1))
      .join(" ");
}

function searchMovie(query: string) : Promise<Paging<Movie>> {
  const tranformedSearchQuery = transformQuery(query);

  console.log('tranformedSearchQuery', tranformedSearchQuery)
  return fetch(process.env.API_URL + `/search/movie/?query=${tranformedSearchQuery}&include_adult=true`, {
    headers: {
      'Authorization' : `Bearer ${process.env.API_TOKEN}`,
      'Content-Type': 'application/json;charset=utf-8'
    }
  }).then(resp => resp.json())
}