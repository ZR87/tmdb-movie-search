const cron = require('node-cron');
import chalk from 'chalk';
const log = console.log;

//  Helper class to get and cache TMDB config
export class ConfigService {

    private static instance: ConfigService;
    private static configCache: any;
    private constructor() { }

    public static getInstance(): ConfigService {
        if (!ConfigService.instance) {
            ConfigService.instance = new ConfigService();
        }
        this.setConfigEveryMidnight();

        return ConfigService.instance;
    }

    public async getConfig() {
        log(chalk.green('getConfig()'));
        if (ConfigService.configCache) {
            log(chalk.green('returning config from cache'));
            return Promise.resolve( ConfigService.configCache)
        } else {
            log(chalk.yellow('...Fetching cache'));
            return await ConfigService.setConfig();
        }
    }


    private static async setConfig() {
        console.info('TMDB config is fetching...')
        const response = await fetch(process.env.API_URL + '/configuration', {
            headers: {
                'Authorization' : `Bearer ${process.env.API_TOKEN}`,
                'Content-Type': 'application/json;charset=utf-8'
            }
        })
        const data = await response.json();
        log(chalk.red('Config saved!'));
        ConfigService.configCache = data

        return data;
    }

    private static setConfigEveryMidnight() {
        console.info( 'Scheduling  config update CRON');
        cron.schedule('0 0 * * *', () => {
            console.log('CRON: Fetching task every day at midnight');
            ConfigService.setConfig();
        });
    }
}