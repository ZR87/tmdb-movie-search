// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const puppeteer = require("puppeteer");
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  name: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {

  const title = req.query.title as string;
  const year = req.query.year as string;
  console.log('query, title', title, year);

  const searchResponse = await fetch(buildQueryUrl(title, year));

  if(searchResponse.ok) {
    const data = await searchResponse.json();
    if (data && data.length >= 0) {
      const wikiLink = data[3][0];
      console.log("wikiLink", wikiLink);

      try {
        const scrapeData = await scrapeWikiArticle(wikiLink);
        if(scrapeData) {
          console.log('scrapeData', scrapeData)
          res.status(200).json(scrapeData);
        }
       }catch(error) {
        res.status(404).json({ message: "Couldn't fetch Wikipedia article" });
      }

    }
  }

}

function buildQueryUrl(title: string, year: string) {
  const titleCapitalized = title.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
  return `https://en.wikipedia.org/w/api.php?action=opensearch&search=${titleCapitalized}&limit=4&namespace=0&format=json`
}

async function scrapeWikiArticle(url: string) {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url );

  const data = await page.evaluate(() => {
    const heading = document.querySelector("#firstHeading").textContent.trim()
    const article = document.querySelector('#bodyContent').textContent.trim();
    return {
      heading,
      article
    }
  });
  console.log('data', data);
  await browser.close();
  return data;
}
