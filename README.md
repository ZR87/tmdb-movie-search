
# TMDB Movies Search
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).
Requirements: node v14 or above

## Getting started
- First, install npm dependencies:

    ```bash
    npm install
    ```

- Copy .env.local.example file to .env.local and fill TMDB API key

   The environemtn file `.env.local` should look liek this
   ```
   API_TOKEN=<tmdb-api-token-goes-here>
   API_URL=https://api.themoviedb.org/3
   ```

- Run `npm run build` command to build , then `npm run start` to serve.

    Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
