import {useSelector} from "react-redux";
import {selectMoviesData, selectMoviesError, selectMoviesLoading } from "../../redux/movieSlice";
import {MovieItem} from "../MovieItem/MovieItem";
import styles from "./MovieList.module.css";
import {Movie, Paging} from "../../models/common";

export const MovieList = () => {
    const movies:  Paging<Movie> = useSelector(selectMoviesData);
    const isloading = useSelector(selectMoviesLoading);
    const error = useSelector(selectMoviesError);

    if (isloading) return (
       <div> ...is loading </div>
    )

    if (error) return (
        <div>
            <span>Opps! Error</span>
        </div>
    )

    return (
        <div className={styles.MovieList_container}>
            {movies && movies?.results?.map((movie) => (
                <MovieItem movie={movie} key={movie.id}/>
            ))}
        </div>
    )
}