import { useRef, useState} from 'react';
import {useDispatch } from 'react-redux'
import {setQuery} from '../../redux/searchSlice';
import {getMoviesAsync} from "../../redux/movieSlice";

const Search = () => {
    const inputRef = useRef();
    const dispatch = useDispatch();
    const [searchTerm, setSearchTerm] = useState('')

    useRef(() => {
        inputRef?.current.focus();
    }, []);

    const handleInputSubmit = (event) => {
        event.preventDefault();
        const query = inputRef.current.value;

        if(searchTerm === query) {
            alert('Oops! Try something different!');
            return;
        }

        if (query.length) {
            dispatch(setQuery(query));
            dispatch(getMoviesAsync(query));
            setSearchTerm(query);
        }

    };



    return (
        <form onSubmit={handleInputSubmit}>
            <input
                ref={inputRef}
                type="search"
                name="q"
                autoComplete="off"
                placeholder="Keresés..."
                required
            />
            <input
                type="button"
                onClick={handleInputSubmit}
                value="Search"
            />
        </form>
    );
};

export default Search;
