import {Movie} from "../../models/common";
import Image from "next/image";
import styles from "./MovieItem.module.css";
import {useDispatch, useSelector} from "react-redux";
import {selectMoviesSelected, setSelectedMovie} from "../../redux/movieSlice";
import {getWikiAsync} from "../../redux/wikiSlice";

interface MovieItemProps {
    movie: Movie;
}

export const MovieItem = ({ movie }: MovieItemProps) => {
    const dispatch = useDispatch();
    // @ts-ignore
    const selectedMovieId = useSelector(selectMoviesSelected);

    const handleClick = (movie: Movie) => {
        if(movie.id === selectedMovieId) {
            dispatch(setSelectedMovie(null));

        } else {
            dispatch(setSelectedMovie(movie.id));
            // @ts-ignore
            dispatch(getWikiAsync({ title: movie.title, date: movie.release_date}))
        }

    }

    return (
        <div className={`${selectedMovieId === movie.id ? styles.selected : ""}`}>
            <Image src={movie.posterURL} layout='responsive' width={256} height={384}></Image>
            <h2
                className={styles.title}
                onClick={() => handleClick(movie)}
            >{movie.title}</h2>
            <span>Release date: {movie.release_date}</span>
        </div>
    )
}