import { createSlice } from '@reduxjs/toolkit'

export const searchSlice = createSlice({
    name: 'search',
    initialState: {
        query: null
    },
    reducers: {
        setQuery: (state, action)  => {
            state.query = action.payload;
        }
    }
})
export const selectSearchQuery = (state: { search: { query: string } }) => state.search.query

// Action creators are generated for each case reducer function
export const { setQuery } = searchSlice.actions

export default searchSlice.reducer;