import { configureStore } from '@reduxjs/toolkit'

import searchReducer from './searchSlice'
import movieReducer from './movieSlice';
import wikiReducer from './wikiSlice';

export default configureStore({
    reducer: {
        search: searchReducer,
        movies: movieReducer,
        wiki: wikiReducer
    },
})