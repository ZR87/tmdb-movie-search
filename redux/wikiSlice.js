import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {HTTP_STATUS} from "../common/constants";

export const getWikiAsync = createAsyncThunk('wiki/getWikiAsync', async (payload) => {
    const year = payload.date.split('-')[0];
    const buildQueryUrl = (payload) => `/api/wiki?title=${payload.title}&year=${year}`

    const response = await fetch(buildQueryUrl(payload))

    if (response.ok) {
        const data = await response.json();
        return { data };
    }
})

export const wikiSlice = createSlice({
    name: 'wiki',
    initialState: {
        loading: null,
        data: null,
        error: null,
    },
    reducers: {},
    extraReducers: {
        [getWikiAsync.pending]: (state) => {
            state.status = HTTP_STATUS.PENDING;
            state.loading = true;
        },
        [getWikiAsync.rejected]: (state, { error }) => {
            state.loading = false;
            state.status = HTTP_STATUS.REJECTED;
            state.error = error.message;
        },
        [getWikiAsync.fulfilled]: (state, action) => {
            state.loading = false;
            state.status = HTTP_STATUS.FULFILLED;
            state.data = action.payload.data
        }
    }

});

// create selectors
export const selectMoviesData = (state) => state.wiki.data;
export const selectWikiLoading = (state) => state.wiki.loading;
export const selectWikiError = (state) => state.wiki.error;

export default wikiSlice.reducer;