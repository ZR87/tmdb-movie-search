import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {HTTP_STATUS} from "../common/constants";

export const getMoviesAsync = createAsyncThunk('movies/getMoviesAsync', async (payload) => {
    const searchEndpoint = (query) => `/api/search?q=${query}`
    const response = await fetch(searchEndpoint(payload))

    if (response.ok) {
        const data = await response.json();
        return { data };
    }
})

export const movieSlice = createSlice({
    name: 'movies',
    initialState: {
        loading: null,
        data: null,
        error: null,
        selected: null
    },
    reducers: {
        setSelectedMovie: (state, action) => {
            state.selected = action.payload;
        }

    },
    extraReducers: {
        [getMoviesAsync.pending]: (state) => {
            state.status = HTTP_STATUS.PENDING;
            state.loading = true;
        },
        [getMoviesAsync.rejected]: (state, { error }) => {
            state.loading = false;
            state.status = HTTP_STATUS.REJECTED;
            state.error = error.message;
        },
        [getMoviesAsync.fulfilled]: (state, action) => {
            state.loading = false;
            state.status = HTTP_STATUS.FULFILLED;
            state.data = action.payload.data
        }
    }

});

// create selectors
export const selectMoviesData= (state) => state.movies.data;
export const selectMoviesLoading = (state) => state.movies.loading;
export const selectMoviesError = (state) => state.movies.error;
export const selectMoviesSelected = (state) => state.movies.selected;

export const { setSelectedMovie } = movieSlice.actions

export default movieSlice.reducer;